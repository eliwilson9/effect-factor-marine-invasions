import pandas as pd
import numpy as np
import warnings

import requests as re

from tqdm import tqdm
from time import sleep


def get_species(name, token):
    """
    get_species(name, token)
    
    Returns a list of dictionaries describing: code, title, timing, scope, severity,
    score, and invasive status of the species in question.
    
    Parameters
    ----------
        name: string
            Scientific (latin) name of species as a string.
        token: string
            A string of your API token from the IUCN red list.
            This can be acquired here: https://apiv3.iucnredlist.org/api/v3/token
    """
    return (
        re.get(
            "https://apiv3.iucnredlist.org/api/v3/threats/species/name/"
            + name
            + "?token="
            + token
        )
        .json()
        .get("result")
    )


def iucn_add_main_categories(df):
    """
    iucn_add_main_categories(df)

    Returns a dataframe where "major codes" are added by the end.
    E.g., if there is the following codes: [1.2.1, 8.1, 8.2] then the
    major codes are added: [1, 8]

    Parameters
    ----------
        df: pandas.DataFrame
            A dataframe with the columns: [species, code]
    """
    empls = []  # empty list of species
    emplc = []  # empty list of codes
    for s, c in zip(
        df.species, np.array(df.code)
    ):  # For species and the code of the species, if the first 3 symbols give a float lower than 10 (max = '9.9') then add only the first digit to the code list emplc.
        empls.append(s)
        if float(c[0:3]) < 10:
            emplc.append(c[0])
        else:  # If not, add the first 2 digits (gives 10, 11, or 12)
            emplc.append(c[0:2])
    newdf = pd.DataFrame(
        columns=["species", "code"]
    )  # Make new DataFrame with species and their codes added.
    newdf.species = empls
    newdf.code = emplc
    newdf = newdf.drop_duplicates()  # Remove the duplicates.
    return newdf

def get_iucn_codes(token, species):
    """
    get_iucn_codes(token, species)

    Makes API calls to the IUCN using the given API-token, and looking for the species
    names in the list of species names given. Then returns a pandas dataframe with each
    species-code combination, along with the following data if available: [timing, scope,
    severity, score, invasive].
    For details on what that data describes, see the two links:
    * https://apiv3.iucnredlist.org/api/v3/docs#threat-name 
    * https://www.iucnredlist.org/resources/threat-classification-scheme 
        
    Parameters
    ----------
        token: string
            Your API token from the IUCN red list.
        species: list
            A list of species in the IUCN red list, e.g. ['Hexanchus griseus', 'Lagocephalus gloveri', 'Lamna nasus']

    OBS!
    ----
    The download may take time, remember to define the dataframe (e.g.: df = get_iucn_codes(token, species)) or you'll have
    to do it all over!
    """
    df = pd.DataFrame()
    not_found_species = []
    # print('When download is complete, write ´df´ for dataframe with species and threat-codes. Write ´not_found_species´ for a list of species not found and added to the dataframe.')
    for species_count, name in zip(
        tqdm(range(len(species))), species
    ):  # A loading bar has been included in the loop to see how many species have been added to the dataframe.
        switch = 0
        while True:
            try:
                resp = get_species(
                    name, token
                )  # Response of API call from the IUCN-API responding the species name while using your token.
            except re.exceptions.Timeout:
                if switch == 0:
                    print(
                        "At species number "
                        + str(species_count)
                        + " there was a timeout error"
                    )
                    switch = 1
                continue
            except re.exceptions.TooManyRedirects:
                print("The URL was bad, try a different one")
            except re.exceptions.ConnectionError:
                if switch == 0:
                    print(
                        "At species number "
                        + str(species_count)
                        + " there has been an error with your connection"
                    )
                    switch = 1
                continue
            break

        if resp != []:  # If there there is a result, add the codes to an empty list.
            codelist = []
            timinglist = []
            scopelist = []
            severitylist = []
            scorelist = []
            invasivelist = []
            for i in range(len(resp)):
                codelist.append(resp[i].get("code"))
                timinglist.append(resp[i].get("timing"))
                scopelist.append(resp[i].get("scope"))
                severitylist.append(resp[i].get("severity"))
                scorelist.append(resp[i].get("score"))
                invasivelist.append(resp[i].get("invasive"))

            dfx = pd.DataFrame(
                columns=[
                    "species",
                    "code",
                    "timing",
                    "scope",
                    "severity",
                    "score",
                    "invasive",
                ]
            )  # Add the list of codes to a dataframe filled with the species' name.
            dfx.code = pd.Series(codelist)
            dfx.timing = pd.Series(timinglist)
            dfx.scope = pd.Series(scopelist)
            dfx.severity = pd.Series(severitylist)
            dfx.score = pd.Series(scorelist)
            dfx.invasive = pd.Series(invasivelist)
            dfx.species = name
            df = pd.concat(
                [df, dfx], ignore_index=True
            )  # Append this dataframe to the final dataframe. Used to be: 'df.append(dfx)' but pd.df.append has been depreciated.
            sleep(1)  # Sleep one second to not misuse the API when scraping.

        else:  # If there is no result, then the species name was not found in the IUCN red list. There may be a spelling mistake or the species is not considered threatened according to IUCN.
            not_found_species.append(name)
    if len(not_found_species) > 0:
        # get_ipython().run_line_magic("pinfo", "UserWarning")
        print("Some species were not found:")
        print(not_found_species)

    if len(df) == 0:
        print("No species have been added to the Dataframe")
        return df
    else:
        df_m = pd.concat(
            [df, iucn_add_main_categories(df)]
        )  # Adds the main categories to the dataframe (so if there is a species with code '8.6' and '10.2', category '8' and '10' will be added.)

        return df_m
