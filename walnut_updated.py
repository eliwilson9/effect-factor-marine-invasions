"""
The IUCN_Heatmap package is used to calculate how much an IUCN threat is represented within a given marine ecoregion of the world (MEOW) (See about MEOW in Spalding et al. 2007).
Based on the assumption that each threat is uniformly distributed within the habitat range of a species, it calculates the threat intensity - a smaller habitat range gives a higher
threat intensity because we are more certain that the threat is really located in the area. Species habitat range is from MarINvaders.

The most common use of the IUCN_Heatmap package is to simply make a list of threat codes (e.g. `codes = ['8.1','8.2','8.3']` ) and use the function `p_RT(codes)`.
The result is the threat represented in each MEOW compared to the other threats in the list. A longer list therefore tends to give smaller values due to normalization on more columns.
If a threat is not present within an ecoregion, the threat is also not represented in the calculation since the the represented threat (p_RT) value is normalized on the summed threat intensities, not the number of threats.
Therefore, if the threat is not present in the MEOW then the threat intensity is 0 which does not add to the sum of threat intensities of which p_RT is normalized to.
"""
# Set up file directory paths here:
iucn_path = "data/iucn/threat-codes_20_02-2023_with_redlistCategory.csv"  # "data/iucn/threat-codes_20_02-2023_with_redlistCategory.csv"  # Must be with 'species' and 'code' (threat) as column names.
sqlite_path = "data\sqlite\marinvaders_08-03-2023.sqlite3"

# Importing packages
import numpy as np
import pandas as pd
import time
import sqlite3
import json

con = sqlite3.connect(sqlite_path)  # Connect to sqlite database
cur = con.cursor()  # Create cursor object

iucn = pd.read_csv(iucn_path)
iucn = iucn.loc[iucn.redlistCategory != "Least Concern"].get(["species", "code"])
iucn_category = pd.read_csv(iucn_path).get(["species", "redlistCategory"])
meow = pd.read_excel("data/heatmap/meow_ecos.xlsx").sort_values(
    by="ECO_CODE", ignore_index=True
)
eco = np.array(meow.ECO_CODE.tolist())


def get_data():
    """Returns two pandas dataframes from the sqlite file added to the script during the Setup_guide.ipynb."""
    df_all_species = pd.read_sql_query(
        "SELECT * FROM all_species", con
    )
    df_overview = pd.read_sql_query("SELECT * FROM overview", con)
    if len(df_overview) == 0:
        raise Exception(
            'The path connecting to the sqlite3 database is not pointing at a sqlite database or an empty database. Remember to add the whole string including file format ".sqlite3".'
        )
    return df_all_species, df_overview

# data includes all species from the MarINvaders database, overview gives species counts of different categories (obis, aliens, threatened) in each ecoregion. 
(
    data,
    overview,
) = (
    get_data()
)

# The all_species dataframe shows witch ecoregion each species is extant in.
all_species = (
    data.drop_duplicates(["aphiaID", "ECO_CODE"])
    .get(["species", "ECO_CODE"])
    .groupby("species")
    .agg(list)
    .reset_index()
)


def iucn_codes_available():
    """A dataframe showing which iucn codes are available. Hint: Use pandas.set_option('display.max_rows', None) to see the whole table."""
    df = pd.read_csv("data/heatmap/iucn_threat_description.csv")
    return df


def species_count():
    """Returns a dataframe with total numbers of species in each ecoregion."""
    S = (
        data.get(["aphiaID", "ECO_CODE"])
        .drop_duplicates("aphiaID")
        .groupby("ECO_CODE")
        .agg(list)
        .reset_index()
    )  # Get the columns with aphiaIDs and ECO_CODEs from the dataset, drop the doublicates and group them by eco codes in a list.
    total_species = pd.DataFrame()  # Making an empty dataframe to concatanate to.
    for i in range(
        len(S)
    ):  # Building a new dataframe which counts the length of the unique aphiaID list, thereby counting number of species found in the ecoregion.
        x = np.array([S.iloc[i, 0], len(S.iloc[i, 1])])
        row = pd.DataFrame([x], columns=["ECO_CODE", "species_count"])
        total_species = pd.concat([total_species, row], axis=0)

    return total_species


def read_iucn(include):
    """
    read_iucn(include)

    Reading the assessment.csv file received from IUCN and returns a pandas
    dataframe showing every combination of threat code and species.
    Therefore, species may be counted multiple times.

    Parameters
    ----------
    include: list
        A list of IUCN threat-codes. The threat-codes are each of string type
        to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """
    iucn_clean = iucn.loc[iucn.code.isin(include)]
    return iucn_clean


def read_meow():
    """
    read_meow()

    Reads the excel file: "data/heatmap/meow_ecos.xlsx" with ECO_CODE, ECOREGION,
    and area of their polygons in km2. The area is precalculated in ArcGIS Pro
    using the geodesic area of each ecoregion, using the equal-area projected
    coordinate system: Equal Earth (sphere).
    Then returns a pandas dataframe with columns: [ECO_CODE, ECOREGION, Areakm2].
    """
    newmeow = (
        meow.get(["ECO_CODE", "ECOREGION", "Areakm2"])
        .sort_values(by=["ECO_CODE"])
        .dropna()
    )
    return newmeow


def read_data(include):
    """
    read_data(include)

    Returns a dataframe with all species affected by the iucn codes included in the
    input list along with their threat code, and ECO_CODE they belong in.
    
    Parameters
    ----------
    include: list
        A list of IUCN threat-codes. The threat-codes are each of string type
        to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """
    iucn_list = read_iucn(include).groupby("species").agg(list).reset_index()
    df = iucn_list.merge(all_species, on="species", how="inner")
    return df

def SR(include):
    df = read_data(include)
    SR = pd.DataFrame(np.zeros((len(df.species), len(eco))), columns = eco, index = np.array(df.species))
    for index, row in df.iterrows():
        for ecocode in row.ECO_CODE:
            SR.loc[row.species, ecocode] = 1
    
    return SR


def ST(include):
    """
    ST(include)

    Species-Threat (ST) matrix. Returns a binomial (yes/no) matrix in the dimensions
    (species, iucn-threats) stating which threat-codes threatens each species.

    Parameters
    ----------
        include: list
            A list of IUCN threat-codes. The threat-codes are each of string type
            to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """
    df = read_data(include)
    ST = pd.DataFrame(np.zeros((len(df), len(include))), columns=np.array(include), index=np.array(df.species))
    for index, row in df.iterrows():
        for threat in row.code:
            ST.loc[row.species, threat] = 1
    return ST


def omega(include):
    """
    omega(include)

    Calculates the total habitat area (km2) of each species by summing together the
    areas of the MEOWs in which it is present. Then returns a vector with total
    habitat area of each species.
    
    Parameters
    ----------
        include: list
            A list of IUCN threat-codes. The threat-codes are each of string type
            to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """
    omega = pd.DataFrame(np.dot(SR(include), np.array(meow['Areakm2'])),
                    index=np.array(SR(include).index), columns = ['omega'])
    return omega


def I_RS(include):
    """
    I_RS(include)

    Intensity of species in each region (I_RS) matrix. Returns a matrix with dimension
    (ecoregions, species). Each indice shows the "species intensity" in the ecoregion
    which is the area of the ecoregion divided by the total habitat range of the species.

    Parameters
    ----------
        include: list
            A list of IUCN threat-codes. The threat-codes are each of string type
            to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """

    area = pd.DataFrame(meow.get('Areakm2'))
    area.set_index(meow.ECO_CODE.values, inplace=True)
    df = read_data(include)
    om = omega(include)
    ecoregion_count = len(eco)
    species_count = len(df)


    RS = pd.DataFrame(np.zeros((ecoregion_count, species_count)),
                        index=np.array(eco), columns=np.array(df.species))

    RS_bool = pd.DataFrame(np.zeros((ecoregion_count, species_count)),
                        index=np.array(eco), columns=np.array(df.species))

    for specimen, ecocodes in zip(df.species, df.ECO_CODE):
        RS_bool.loc[RS_bool.index.isin(ecocodes), specimen] = 1

    RS_bool.replace(0, np.nan, inplace=True)

    for species, habitat_range in om.iterrows():
        RS[species] = RS_bool[species]*habitat_range.values
        
    for ecocode, km2 in area.iterrows():
        RS.loc[ecocode] = km2.values / RS.loc[ecocode].values

    RS.replace(np.nan, 0, inplace=True)
    return RS
"""

    Zm_IRS = np.zeros(
        (ecoregion_count, species_count)
    )

    for r in range(ecoregion_count):  # For every region
        for s in range(species_count):  # And every species in this region
            if (
                eco[r] in df.iloc[s, 2]
            ):  # If the ecoregion number can be seen in the species' dataframe (the species is present in this ecoregion)
                Zm_IRS[r, s] = (
                    area[r] / om[s]
                )  # Then we add a value of the area of this ecoregion divided by the total area of the species' habitat range (usually multiple ecoregions summed)

    return Zm_IRS

"""


def p_RT(include):
    """
    p_RT(include)

    Calculates the probability that the threatened species in this MEOW is threatened by each
    threat code compared to the other threats. Then returns a matrix with dimension
    (ecoregions, threats). Each indice shows how much a threat is represented in each ecoregion.

    Parameters
    ----------
        include: list
            A list of IUCN threat-codes. The threat-codes are each of string type
            to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
    """

    start_time = time.time()
    I_RT= np.dot(I_RS(include), ST(include))
    I_RT = pd.DataFrame(I_RT, index=I_RS(include).index, columns=ST(include).columns)

    pRT = pd.DataFrame(np.zeros(I_RT.shape), index=I_RT.index, columns=I_RT.columns)

    for ecocode, row in I_RT.iterrows():  # For every region
        rowsum = np.sum(row.values)
        if rowsum == 0:  # If the sum of the threat intensities are 0
            print(f"Ecoregion {ecocode} has zero species threatened by the given threat codes")  # let me know that this ecoregion has no species affected by this threat
        else:
            pRT.loc[ecocode] = row.values/rowsum
    print("--- Calculation time was %s seconds ---" % (time.time() - start_time))
    return pRT


def p_RT_to_excel(include, filename):
    """
    p_RT_to_excel(include, filename)

    Runs the p_RT function with the included threat-codes and creates an excel file
    in your current working directory with the p_RT matrix.

    Parameters
    ----------
        include: list
            A list of IUCN threat-codes. The threat-codes are each of string type
            to allow for multiple "dots". Example input: ['1', '1.1', '8', '8.1.1'].
        
        filename: string
            string with name of file (don't include .xlsx)
    """
    import os

    p_RT(include).to_excel(filename + ".xlsx")
    first_df = pd.read_excel(filename + ".xlsx").rename(
        columns={"Unnamed: 0": "ECO_CODE"}
    )
    final_df = read_meow().merge(first_df, on="ECO_CODE", how="inner")
    final_df.to_excel(filename + ".xlsx")
    print(
        f"The p_RT dataframe has been saved in the folder {os.getcwd()}"
        + " as "
        + filename
        + ".xlsx"
    )


def ecoregion(eco_code):
    """
    ecoregion(eco_code)

    Returns a .json dump including aliens, aliensSightings, and obis dictionaries.

    parameteters
    ------
    eco_code: int
        The ecocode of the ecoregion whose data you would like to return.
    ...

    >>> ecoregion(20022)
    """
    sql = f"SELECT aphiaID, species, dataset FROM all_species WHERE ECO_CODE = {eco_code} GROUP BY aphiaID;"
    obis = [
        {"aphiaID": species[0], "species": species[1], "dataset": species[2]}
        for species in cur.execute(sql)
    ]

    sql = f"SELECT aphiaID, species, dataset FROM aliens WHERE ECO_CODE = {eco_code} GROUP BY aphiaID;"
    aliens = [
        {"aphiaID": species[0], "species": species[1], "dataset": species[2]}
        for species in cur.execute(sql)
    ]

    sql = f"SELECT aphiaID, species FROM affected_by_invasive WHERE ECO_CODE = {eco_code} GROUP BY aphiaID;"
    threatened = [
        {"aphiaID": species[0], "species": species[1]} for species in cur.execute(sql)
    ]

    obis_ids = [x["aphiaID"] for x in obis]
    aliens_obis = [x for x in aliens if x["aphiaID"] in obis_ids]
    # we assume t have obis data always valid and not empty since all is based on OBIS
    if obis:
        ret = {
            "response": 200,
            "aliens": aliens,
            "obis": obis,
            "aliensSightings": aliens_obis,
            "len_obis": len(obis),
            "len_aliens": len(aliens),
            "len_aliensSightings": len(
                aliens_obis
            ),  # This is count of alien sightings in an ecoregion.
            "affected": threatened,
        }
    else:
        ret = {"response": 404, "text": "No records found for selected region."}

    return json.dumps(ret)
